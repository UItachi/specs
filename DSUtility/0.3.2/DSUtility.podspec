#
# Be sure to run `pod lib lint DSUtility.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged˙
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DSUtility'
  s.version          = '0.3.2'
  s.summary          = 'A serials of class utilities by Dragon Source.'
  s.description      = <<-DESC
View, Category, Helper class, a lot of utilities in it.
                       DESC

  s.homepage         = 'https://bitbucket.org/UItachi/dsutility'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Will Han' => 'xingheng.hax@qq.com' }
  s.source           = { :git => 'https://bitbucket.org/UItachi/dsutility.git', :tag => s.version.to_s }
  s.social_media_url = 'https://github.com/xingheng'

  s.ios.deployment_target = '8.0'
  s.frameworks = 'UIKit'

  s.default_subspec = 'All'
  s.subspec 'All' do |ss|
    ss.ios.dependency 'DSUtility/Core'
    ss.ios.dependency 'DSUtility/String'
    ss.ios.dependency 'DSUtility/Image'
    ss.ios.dependency 'DSUtility/Path'
    ss.ios.dependency 'DSUtility/Date'
    ss.ios.dependency 'DSUtility/View'
    ss.ios.dependency 'DSUtility/TableView'
    ss.ios.dependency 'DSUtility/CustomView'
    ss.ios.dependency 'DSUtility/ViewController'
    ss.ios.dependency 'DSUtility/Encryption'
  end

  s.subspec 'Core' do |ss|
    ss.source_files = 'DSUtility/Core/**/*'
    ss.public_header_files = 'DSUtility/Core/*.h'
  end

  s.subspec 'String' do |ss|
    ss.source_files = 'DSUtility/String/**/*'
    ss.public_header_files = 'DSUtility/String/*.h'
  end

  s.subspec 'Image' do |ss|
    ss.source_files = 'DSUtility/Image/**/*'
    ss.public_header_files = 'DSUtility/Image/*.h'
  end

  s.subspec 'Path' do |ss|
    ss.source_files = 'DSUtility/Path/**/*'
    ss.public_header_files = 'DSUtility/Path/*.h'
  end

  s.subspec 'Date' do |ss|
    ss.source_files = 'DSUtility/Date/**/*'
    ss.public_header_files = 'DSUtility/Date/*.h'
  end

  s.subspec 'View' do |ss|
    ss.source_files = 'DSUtility/View/**/*'
    ss.public_header_files = 'DSUtility/View/*.h'
  end

  s.subspec 'TableView' do |ss|
    ss.source_files = 'DSUtility/TableView/**/*'
    ss.public_header_files = 'DSUtility/TableView/*.h'
  end

  s.subspec 'CustomView' do |ss|
    ss.dependency 'DSUtility/Core'
    ss.source_files = 'DSUtility/CustomView/**/*'
    ss.public_header_files = 'DSUtility/CustomView/*.h'
  end

  s.subspec 'ViewController' do |ss|
    ss.source_files = 'DSUtility/ViewController/**/*'
    ss.public_header_files = 'DSUtility/ViewController/*.h'
  end

  s.subspec 'Encryption' do |ss|
    ss.source_files = 'DSUtility/Encryption/**/*'
    ss.public_header_files = 'DSUtility/Encryption/*.h'
  end

end
