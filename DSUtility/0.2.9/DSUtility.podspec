#
# Be sure to run `pod lib lint DSUtility.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged˙
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DSUtility'
  s.version          = '0.2.9'
  s.summary          = 'A serials of class utilities by Dragon Source.'
  s.description      = <<-DESC
View, Category, Helper class, a lot of utilities in it.
                       DESC

  s.homepage         = 'https://bitbucket.org/UItachi/dsutility'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Will Han' => 'xingheng.hax@qq.com' }
  s.source           = { :git => 'https://bitbucket.org/UItachi/dsutility.git', :tag => s.version.to_s }
  s.social_media_url = 'https://github.com/xingheng'

  s.ios.deployment_target = '8.0'

  s.source_files = 'DSUtility/**/*'

  s.public_header_files = 'DSUtility/**/*.h', 'DSUtility/DSUtility.h'
  s.frameworks = 'UIKit'

  s.prefix_header_contents = <<-EOS
    #import "DSUtility.h"
  EOS
end
