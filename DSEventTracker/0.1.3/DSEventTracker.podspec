
Pod::Spec.new do |s|
  s.name         = "DSEventTracker"
  s.version      = "0.1.3"
  s.summary      = "An event tracker kit for DragonSource's iOS products."

  s.description  = "DSEventTracker sends and mananges the specified events to our DAS backend server."

  s.homepage     = "https://bitbucket.org/dragonsource/dseventtracker"
  s.license      = "Copyright @ DragonSource"

  s.author             = { "Will Han" => "xingheng907@hotmail.com" }
  s.social_media_url   = "https://github.com/xingheng"

  s.platform     = :ios, "8.0"
  s.source       = { :git => 'https://bitbucket.org/dragonsource/dseventtracker.git', :tag => s.version.to_s }

  s.source_files        = "DSEventTracker", "DSEventTracker/**/*.{h,m}"
  s.public_header_files = "DSEventTracker/**/*.h"
  s.prefix_header_contents = '#import <Foundation/Foundation.h>', <<-DESC
      // ------------------------------ BEGIN ------------------------------
      // Place the log macro definition on the top of following dependency.
      #ifndef DDLogMacro

      #define DDLogError(frmt, ...)   NSLog(frmt, ## __VA_ARGS__)
      #define DDLogWarn(frmt, ...)    NSLog(frmt, ## __VA_ARGS__)
      #define DDLogInfo(frmt, ...)    NSLog(frmt, ## __VA_ARGS__)
      #define DDLogDebug(frmt, ...)   NSLog(frmt, ## __VA_ARGS__)
      #define DDLogVerbose(frmt, ...) NSLog(frmt, ## __VA_ARGS__)

      #else

      #import <CocoaLumberjack/CocoaLumberjack.h>
      #define ddLogLevel DDLogMacro

      #endif /* DDLogMacro */

      #import <libextobjc/extobjc.h>
      #import <BlocksKit/BlocksKit.h>
      #import <DSUtility/DSUtility.h>
      // -------------------------------  END -------------------------------
  DESC

  s.requires_arc = true
  s.dependency 'libextobjc', '~> 0.4.1'
  s.dependency 'BlocksKit/Core', '~> 2.2.5'
  s.dependency "DerivedRequest", '~> 0.1'
  s.dependency 'DSUtility/Core', '~> 0.3.3'

end
