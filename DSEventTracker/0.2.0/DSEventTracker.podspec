
Pod::Spec.new do |s|
  s.name         = "DSEventTracker"
  s.version      = "0.2.0"
  s.summary      = "An event tracker kit for DragonSource's iOS products."

  s.description  = "DSEventTracker sends and mananges the specified events to our DAS backend server."

  s.homepage     = "https://bitbucket.org/dragonsource/dseventtracker"
  s.license      = "Copyright @ DragonSource"

  s.author             = { "Will Han" => "xingheng907@hotmail.com" }
  s.social_media_url   = "https://github.com/xingheng"

  s.platform     = :ios, "8.0"
  s.source       = { :git => 'https://bitbucket.org/dragonsource/dseventtracker.git', :tag => s.version.to_s }

  s.source_files        = "DSEventTracker/**/*.{h,m}"
  s.public_header_files = "DSEventTracker/**/*.h"
  s.prefix_header_contents = '#import <Foundation/Foundation.h>', <<-DESC
      // ------------------------------ BEGIN ------------------------------
      // Place the log macro definition on the top of following dependency.
      #import <libextobjc/extobjc.h>
      #import <BlocksKit/BlocksKit.h>
      #import <DSUtility/DSUtility.h>
      #import <DSUtility/DSLog.h>

      #ifndef DS_LOG_LEVEL_DEF
      #define DS_LOG_LEVEL_DEF eventLogLevel
      #endif

      FOUNDATION_EXPORT DDLogLevel eventLogLevel;
      // -------------------------------  END -------------------------------
  DESC

  s.requires_arc = true
  s.dependency 'libextobjc', '~> 0.4'
  s.dependency 'BlocksKit/Core', '~> 2.2.5'
  s.dependency 'DerivedRequest', '~> 0.1'
  s.dependency 'DSUtility/Core'
  s.dependency 'DSUtility/Logger'

end
