#
# Be sure to run `pod lib lint DSEpubReader.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DSEpubReader'
  s.version          = '0.4.0'
  s.summary          = 'DSEpubReader is a core framework of DragonSource for analyzing and displaying the private ePub document.'

  s.description      = <<-DESC
The initial intention of this library is to display the private ePub document from DragonSource, that is, the only one supported document format is our private definition. Do not try to open the ePub standard document with your expected result.
                       DESC

  s.homepage         = 'https://bitbucket.org/dragonsource/dsepubreader'
  s.license          = { :type => 'DragonSource Inc.', :file => 'LICENSE.md' }
  s.author           = { 'Will Han' => 'xingheng.hax@qq.com' }
  s.source           = { :git => 'https://bitbucket.org/dragonsource/dsepubreader.git', :tag => s.version.to_s }
  s.social_media_url = 'http://weibo.com/iqikan'

  s.ios.deployment_target = '8.0'

  s.source_files     = 'DSEpubReader/**/*.{h,m}'
  s.public_header_files = 'DSEpubReader/**/*.h'
  s.prefix_header_file = 'DSEpubReader/DSEpubReader.h'
  s.frameworks       = 'UIKit', 'MediaPlayer', 'QuartzCore', 'AVFoundation'

  # For OWCoreText
  s.libraries        = 'z', 'xml2'
  s.xcconfig         = { 'LIBRARY_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2',
   'HEADER_SEARCH_PATHS' =>  '"$(SDKROOT)/usr/include/libxml2" ' + '"$(PODS_ROOT)" ' + '"$(PODS_ROOT)/BlocksKit" ' + '"$(PODS_ROOT)/DSBaseViewController" ' + '"$(PODS_ROOT)/DSEpubReader" ' + '"$(PODS_ROOT)/DSUtility" ' + '"$(PODS_ROOT)/Masonry" ' + '"$(PODS_ROOT)/HUDHelper" ' + '"$(PODS_ROOT)/OWCoreText" ' + '"$(PODS_ROOT)/OWKit" ' + '"$(PODS_ROOT)/UserDefaultsHelper" ' + '"$(PODS_ROOT)/libextobjc" ' + '"$(PODS_ROOT)/FMDB" ' + '"$(PODS_ROOT)/MBProgressHUD" ' + '"$(PODS_ROOT)/UITextView+Placeholder" ' + '"$(PODS_ROOT)/SSZipArchive" ' + '"$(PODS_ROOT)/libextobjc" ',
   'OTHER_LDFLAGS' => '-lxml2'
  }

  s.resource_bundles = { 'DSEpubReaderAssets' => ['Resources/DSEpubReaderAssets.xcassets/*.imageset/*.png', 'Resources/DSEpubReaderAssets.xcassets', 'Resources/Styles/day.bundle', 'Resources/Styles/night.bundle']
  }

  s.dependency 'libextobjc', '~> 0.4.1'
  s.dependency 'BlocksKit/Core', '~> 2.2.5'
  s.dependency 'Masonry', '~> 1.0.2'
  s.dependency 'SSZipArchive', '~> 1.4'
  s.dependency 'FMDB', '~> 2.6.2'
  s.dependency 'RETableViewManager', '~> 1.7'
  s.dependency 'UITextView+Placeholder'
  s.dependency 'DSUtility', '~> 0.2.4'
  s.dependency 'UserDefaultsHelper', '~> 0.1.1'
  s.dependency 'DSBaseViewController', '~> 1.2.0'
  s.dependency 'HUDHelper', '~> 0.3.6'

  s.dependency 'OWCoreText', '0.2.0'

end
