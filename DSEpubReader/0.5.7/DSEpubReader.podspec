#
# Be sure to run `pod lib lint DSEpubReader.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DSEpubReader'
  s.version          = '0.5.7'
  s.summary          = 'DSEpubReader is a core framework of DragonSource for analyzing and displaying the private ePub document.'

  s.description      = <<-DESC
The initial intention of this library is to display the private ePub document from DragonSource, that is, the only one supported document format is our private definition. Do not try to open the ePub standard document with your expected result.
                       DESC

  s.homepage         = 'https://bitbucket.org/dragonsource/dsepubreader'
  s.license          = { :type => 'DragonSource Inc.', :file => 'LICENSE.md' }
  s.author           = { 'Will Han' => 'xingheng.hax@qq.com' }
  s.source           = { :git => 'https://bitbucket.org/dragonsource/dsepubreader.git', :tag => s.version.to_s }
  s.social_media_url = 'http://weibo.com/iqikan'

  s.platform = :ios
  s.ios.deployment_target = '8.0'
  s.static_framework = true
  # s.private_header_files = 'Private/*'

  s.default_subspecs = 'EpubReader'

  s.subspec 'CoreModel' do |core|
    core.prefix_header_file = 'Private/CoreModel-Prefix.h'
    core.source_files  = 'CoreModel/**/*.{h,m}', core.prefix_header_file
    core.public_header_files = 'CoreModel/**/*.h'
    core.dependency 'BlocksKit/Core', '~> 2.2.5'
    core.dependency 'JSONModel', '~> 1.8'
    core.dependency 'DSUtility', '~> 0.5.3'
  end

  s.subspec 'Storage' do |storage|
    storage.prefix_header_file = 'Private/Storage-Prefix.h'
    storage.source_files  = 'Storage/**/*.{h,m}', 'Private/Headers/*.h', storage.prefix_header_file
    storage.public_header_files = 'Storage/**/*.h'
    storage.private_header_files = 'Private/Headers/*.h'
    storage.dependency 'DSEpubReader/CoreModel'
    storage.dependency 'libextobjc', '~> 0.6'
    storage.dependency 'FMDB', '~> 2.6.2'
    storage.dependency 'SSZipArchive', '~> 1.4'
    storage.dependency 'DSPathManager', '~> 0.1.0'
  end

  s.subspec 'EpubReader' do |epub|
    epub.prefix_header_file = 'Private/EpubReader-Prefix.h'
    epub.source_files     = 'DSEpubReader/**/*.{h,m}', epub.prefix_header_file
    epub.public_header_files = 'DSEpubReader/**/*.h'
    epub.frameworks       = 'UIKit', 'MediaPlayer', 'QuartzCore', 'AVFoundation'

    # For OWCoreText
    epub.libraries        = 'z', 'xml2'
    epub.xcconfig         = { 'LIBRARY_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2',
     'HEADER_SEARCH_PATHS' =>  '"$(SDKROOT)/usr/include/libxml2" ' + '"$(PODS_ROOT)" ' + '"$(PODS_ROOT)/BlocksKit" ' + '"$(PODS_ROOT)/DSBaseViewController" ' + '"$(PODS_ROOT)/DSEpubReader" ' + '"$(PODS_ROOT)/DSUtility" ' + '"$(PODS_ROOT)/Masonry" ' + '"$(PODS_ROOT)/HUDHelper" ' + '"$(PODS_ROOT)/OWCoreText" ' + '"$(PODS_ROOT)/UserDefaultsHelper" ' + '"$(PODS_ROOT)/libextobjc" ' + '"$(PODS_ROOT)/FMDB" ' + '"$(PODS_ROOT)/MBProgressHUD" ' + '"$(PODS_ROOT)/UITextView+Placeholder" ' + '"$(PODS_ROOT)/SSZipArchive" ' + '"$(PODS_ROOT)/libextobjc" ',
     'OTHER_LDFLAGS' => '-lxml2'
    }

    epub.resource_bundles = { 'DSEpubReaderAssets' => ['Assets/DSEpubReaderAssets.xcassets/*.imageset/*.png', 'Assets/DSEpubReaderAssets.xcassets', 'Assets/Styles/day.bundle', 'Assets/Styles/night.bundle']
    }

    epub.dependency 'DSEpubReader/Storage'
    epub.dependency 'Masonry', '~> 1.1.0'
    epub.dependency 'DSBaseViewController', '~> 1.4.1'
    epub.dependency 'RETableViewManager', '~> 1.7'
    epub.dependency 'UITextView+Placeholder'
    epub.dependency 'UserDefaultsHelper', '~> 0.1.1'
    epub.dependency 'HUDHelper', '~> 0.3.6'
    epub.dependency 'OWCoreText', '~> 0.2.4'
  end

  s.subspec 'MagazineReader' do |magazine|
    magazine.prefix_header_file = 'Private/MagazineReader-Prefix.h'
    magazine.source_files     = 'DSMagazineReader/**/*.{h,m}', magazine.prefix_header_file
    magazine.public_header_files = 'DSMagazineReader/**/*.h'
    magazine.frameworks       = 'UIKit'

    magazine.resource_bundles = {
      'DSMagazineReaderAsset' => ['Assets/DSMagazineReaderAsset.xcassets']
    }

    magazine.dependency 'DSEpubReader/Storage'
    magazine.dependency 'Masonry', '~> 1.1.0'
    magazine.dependency 'DSBaseViewController', '~> 1.4.1'
  end

end
