Pod::Spec.new do |s|
  s.name     = 'OWCoreText'
  s.version  = '0.1.0'
  s.license  = 'MIT'
  s.summary  = 'A CoreText framework.'
  s.homepage = 'http://www.dragonsource.com'
  s.author   = { 'grenlight' => 'grenlight@icloud.com' }
  s.source   = { :git => 'https://bitbucket.org/dragonsource/owcoretext.git', :tag => s.version.to_s }

  s.source_files      = 'OWCoreText/**/*.{h,m}'
  s.prefix_header_file = 'OWCoreText/OWCoreText-Prefix.pch'
  s.requires_arc      = true

  s.dependency 'OWKit', '~> 0.1'

  s.platform          = :ios , "8.0"
  s.frameworks        = 'CoreGraphics', 'QuartzCore', 'CoreText'
  s.libraries         = 'z', 'xml2'
  s.xcconfig          = { 'LIBRARY_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2', 'HEADER_SEARCH_PATHS' => '$(SDKROOT)/usr/include/libxml2', 'CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES' => 'YES' }
end