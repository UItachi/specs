#
# Be sure to run `pod lib lint DSAudioPlayer.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DSAudioPlayer'
  s.version          = '0.1.8'
  s.summary          = 'DSAudioPlayer used for play audios for DS Products.'

  s.description      = <<-DESC
  DSAudioPlayer used for play audios for DS Products.
                       DESC

  s.homepage         = 'https://bitbucket.org/dragonsource/dsaudioplayer'
  s.author           = { 'xiangGuang Zhang' => 'zhangx7635093@163.com' }
  s.source           = { :git => 'https://bitbucket.org/dragonsource/dsaudioplayer.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'DSAudioPlayer/Classes/**/*.{h,m}'
  s.public_header_files = 'DSAudioPlayer/Classes/**/*.h'
  s.prefix_header_file = 'DSAudioPlayer/Classes/DSAudioPlayer.h'
  s.frameworks = 'UIKit', 'MediaPlayer', 'AVFoundation'

  s.resource_bundles = {
    'DSAudioPlayerAssets' => ['DSAudioPlayer/images.xcassets/*.imageset/*.png', 'DSAudioPlayer/images.xcassets']
  }

  s.dependency 'libextobjc', '~> 0.4.1'
  s.dependency 'BlocksKit/Core', '~> 2.2.5'
  s.dependency 'Masonry', '~> 1.1.0'
  s.dependency 'FMDB', '~> 2.6'
  s.dependency 'DSUtility', '~> 0.4.1'
  s.dependency 'DSBaseViewController', '1.3.0'
  s.dependency 'HUDHelper', '~> 0.3.7'
  s.dependency 'DOUAudioStreamer', '~> 0.2.15'
  s.dependency 'JSONModel', '~> 1.7'
  s.dependency 'SDWebImage', '~> 4.1.2'
  s.dependency 'DSEventTracker'
  s.dependency 'DerivedRequest', '~> 0.1.2'
  s.dependency 'DSPathManager', '~> 0.1.1'


end
