#
# Be sure to run `pod lib lint DSPathManager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DSPathManager'
  s.version          = '0.1.0'
  s.summary          = 'Path family\'s manager.'
  s.description      = 'Easy to create and manage the shared paths for DragonSource family.'

  s.homepage         = 'https://bitbucket.org/dragonsource/dspathmanager'
  s.license          = { :type => 'dragonsource' }
  s.author           = { 'Will Han' => 'xingheng907@hotmail.com' }
  s.source           = { :git => 'https://bitbucket.org/dragonsource/dspathmanager.git', :tag => s.version.to_s }
  s.social_media_url = 'https://twitter.com/xingheng907'

  s.ios.deployment_target = '8.0'

  s.source_files = 'DSPathManager/Classes/**/*'

  s.public_header_files = 'DSPathManager/Classes/**/*.h'
  s.frameworks = 'Foundation'
end
